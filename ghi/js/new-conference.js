window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/locations/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            console.log(response.status, response.statusText);

        } else {
            const data = await response.json();
            const selectTag = document.getElementById('location');
            for (let location of data.locations) {
                const option = document.createElement('option')
                option.value = location.id // this needs to be location ID. need to modify API to include
                option.innerHTML = location.name // does this match with data?
                selectTag.appendChild(option)
            }
        }
    } catch (e) {
        console.log("error", e);
    }
        });

        const formTag = document.getElementById('create-conference-form');
        formTag.addEventListener('submit', async event => {
          event.preventDefault();
          const formData = new FormData(formTag);
          const json = JSON.stringify(Object.fromEntries(formData));
          const conferenceUrl = 'http://localhost:8000/api/conferences/';
          const fetchConfig = {
            method: "post",
            body: json,
            headers: {
              'Content-Type': 'application/json',
            },
          };
          const response = await fetch(conferenceUrl, fetchConfig);
          if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
          }
    });
